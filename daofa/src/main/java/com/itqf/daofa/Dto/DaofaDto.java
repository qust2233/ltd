package com.itqf.daofa.Dto;

import lombok.Data;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:30
 * description:
 */
@Data

public class DaofaDto {
    private String dfbh;
    private String dfip;
}
