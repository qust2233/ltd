package com.itqf.daofa.Dto;

import lombok.Data;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 12:49
 * description:
 */
@Data
public class DfxyDto {
    private Integer csaccount;
    private String csname;
}
