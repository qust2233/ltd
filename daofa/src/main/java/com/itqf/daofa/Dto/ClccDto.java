package com.itqf.daofa.Dto;

import lombok.Data;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 11:39
 * description:
 */
@Data
public class ClccDto {
    private String cid;
}
