package com.itqf.daofa.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.daofa.Dto.DaofaDto;
import com.itqf.daofa.pojo.Cljc;
import com.itqf.daofa.pojo.Daofa;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 17:23
 * description:
 */
public interface  CljcMapper extends BaseMapper<Cljc> {

}
