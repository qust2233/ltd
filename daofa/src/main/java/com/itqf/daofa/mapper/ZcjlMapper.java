package com.itqf.daofa.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.daofa.pojo.Zcjl;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 15:41
 * description:
 */
public interface ZcjlMapper extends BaseMapper<Zcjl> {
}

