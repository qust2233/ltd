package com.itqf.daofa.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.daofa.pojo.Clcc;
import com.itqf.daofa.pojo.Cljc;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 11:40
 * description:
 */
public interface ClccMapper  extends BaseMapper<Clcc> {
}
