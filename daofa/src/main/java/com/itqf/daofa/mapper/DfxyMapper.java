package com.itqf.daofa.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.daofa.pojo.Daofa;
import com.itqf.daofa.pojo.Dfxy;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 12:50
 * description:
 */
public interface DfxyMapper extends BaseMapper<Dfxy> {
}
