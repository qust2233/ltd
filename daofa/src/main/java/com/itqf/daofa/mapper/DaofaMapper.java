package com.itqf.daofa.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.daofa.Dto.DaofaDto;
import com.itqf.daofa.pojo.Daofa;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:47
 * description:
 */
public interface DaofaMapper extends BaseMapper<Daofa> {



    List<Daofa> findlist(DaofaDto daofaDto);
}
