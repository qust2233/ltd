package com.itqf.daofa.Service.impl;

import com.itqf.daofa.Service.DaofaService;
import com.itqf.daofa.Service.ZcjlService;
import com.itqf.daofa.mapper.DaofaMapper;
import com.itqf.daofa.mapper.ZcjlMapper;
import com.itqf.daofa.pojo.Zcjl;
import com.itqf.daofa.vo.R;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 15:28
 * description:
 */
@Service
@MapperScan("com.itqf.daofa.mapper")
public class ZcjlServiceImpl implements ZcjlService {
    @Autowired
    private ZcjlMapper zcjlMapper;
    @Override
    public R save(Zcjl zcjl) {
        int rows = 0;
        try{
            rows = zcjlMapper.insert(zcjl);
        }catch (Exception e){

        }

        if(rows == 0){
            return R.FAIL("插入数据失败!参数异常!");
        }
        return R.OK("插入数据成功!");
    }
}
