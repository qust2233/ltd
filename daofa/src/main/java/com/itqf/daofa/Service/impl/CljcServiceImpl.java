package com.itqf.daofa.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.daofa.Dto.CljcDto;
import com.itqf.daofa.Service.CljcService;
import com.itqf.daofa.mapper.CljcMapper;
import com.itqf.daofa.pojo.Cljc;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 17:09
 * description:
 *
 */
@Service

public class CljcServiceImpl implements CljcService {

    @Autowired
    private CljcMapper cljcMapper;


    @Override
    public R list(CljcDto cljcDto) {

        QueryWrapper<Cljc> queryWrapper = new QueryWrapper<>();


        if (cljcDto.getCid() != null) {

            queryWrapper.like("cid", cljcDto.getCid());
        }


        List<Cljc> cljcs = cljcMapper.selectList(queryWrapper);

        return R.OK("查询成功", cljcs);
    }
}



