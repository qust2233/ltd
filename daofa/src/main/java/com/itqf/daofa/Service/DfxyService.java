package com.itqf.daofa.Service;

import com.itqf.daofa.Dto.CljcDto;
import com.itqf.daofa.Dto.DfxyDto;
import com.itqf.daofa.vo.R;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 12:50
 * description:
 */
public interface DfxyService {
    R dlist(DfxyDto dfxyDto);
}
