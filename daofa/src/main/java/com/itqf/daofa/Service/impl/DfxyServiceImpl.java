package com.itqf.daofa.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.daofa.Dto.DfxyDto;
import com.itqf.daofa.Service.DaofaService;
import com.itqf.daofa.Service.DfxyService;
import com.itqf.daofa.mapper.DfxyMapper;
import com.itqf.daofa.mapper.ZcjlMapper;
import com.itqf.daofa.pojo.Cljc;
import com.itqf.daofa.pojo.Dfxy;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 12:51
 * description:
 */
@Service
public class DfxyServiceImpl implements DfxyService {
    @Autowired
    private DfxyMapper dfxyMapper;
    @Override
    public R dlist(DfxyDto dfxyDto) {
        QueryWrapper<Dfxy> queryWrapper = new QueryWrapper<>();


        if (dfxyDto.getCsaccount() != null) {

            queryWrapper.like("csaccount", dfxyDto.getCsaccount());
        }


        if(dfxyDto.getCsname() != null){

            queryWrapper.like("csname",dfxyDto.getCsname());

        }
        List<Dfxy> dfxies = dfxyMapper.selectList(queryWrapper);

        return R.OK("查询成功", dfxies);
    }
}
