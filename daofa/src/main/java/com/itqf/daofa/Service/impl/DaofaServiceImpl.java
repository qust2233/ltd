package com.itqf.daofa.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.daofa.Dto.DaofaDto;
import com.itqf.daofa.Service.DaofaService;
import com.itqf.daofa.mapper.DaofaMapper;
import com.itqf.daofa.pojo.Cljc;
import com.itqf.daofa.pojo.Daofa;
import com.itqf.daofa.vo.R;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:59
 * description:
 */

@Service
@MapperScan("com.itqf.daofa.mapper")
public class DaofaServiceImpl implements DaofaService {
    @Autowired
    private DaofaMapper daofaMapper;
    //删除
    @Override
    public R delete(int did) {
        Daofa daofa = new Daofa();
        daofa.setDid(did);
        daofa.setDeleted(1);

        int row = daofaMapper.updateById(daofa);

        return R.OK("数据删除成功!");
    }
//增加
    @Override
    public R save(Daofa daofa) {
        int rows = 0;
        try{
            rows = daofaMapper.insert(daofa);
        }catch (Exception e){

        }

        if(rows == 0){
            return R.FAIL("插入数据失败!参数异常!");
        }
        return R.OK("插入数据成功!");
    }
//查询
    @Override
    public R findlist(DaofaDto daofaDto) {
        QueryWrapper<Daofa> queryWrapper = new QueryWrapper<>();


        if(daofaDto.getDfbh()!= null){

            queryWrapper.like("dfbh",daofaDto.getDfbh());

        }

        if(daofaDto.getDfip()!= null){

            queryWrapper.like("dfip",daofaDto.getDfip());

        }




        List<Daofa> daofas = daofaMapper.selectList(queryWrapper);

        return R.OK("查询成功",daofas);
    }

}
