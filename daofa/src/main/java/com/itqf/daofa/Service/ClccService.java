package com.itqf.daofa.Service;

import com.itqf.daofa.Dto.ClccDto;
import com.itqf.daofa.Dto.CljcDto;
import com.itqf.daofa.vo.R;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 11:40
 * description:
 */
public interface ClccService {
    R clist(ClccDto clccDto);
}
