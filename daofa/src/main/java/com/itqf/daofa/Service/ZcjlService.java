package com.itqf.daofa.Service;

import com.itqf.daofa.pojo.Daofa;
import com.itqf.daofa.pojo.Zcjl;
import com.itqf.daofa.vo.R;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 15:22
 * description:
 */
public interface ZcjlService {
    /**
     * 添加数据方法
     *
     * @param zcjl
     * @return
     */
    R save(Zcjl zcjl);

}
