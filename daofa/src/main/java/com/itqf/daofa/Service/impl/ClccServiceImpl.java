package com.itqf.daofa.Service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.daofa.Dto.ClccDto;
import com.itqf.daofa.Service.ClccService;
import com.itqf.daofa.Service.CljcService;
import com.itqf.daofa.mapper.ClccMapper;

import com.itqf.daofa.pojo.Clcc;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 11:41
 * description:
 */
@Service
public class ClccServiceImpl implements ClccService {
    @Autowired
    private ClccMapper clccMapper;
    @Override
    public R clist(ClccDto clccDto) {



            QueryWrapper<Clcc> queryWrapper = new QueryWrapper<>();


            if (clccDto.getCid() != null) {

                queryWrapper.like("cid", clccDto.getCid());
            }


            List<Clcc> clccs = clccMapper.selectList(queryWrapper);

            return R.OK("查询成功",clccs);
}}
