package com.itqf.daofa.Service;

import com.itqf.daofa.Dto.CljcDto;
import com.itqf.daofa.vo.R;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 16:57
 * description:
 */
public interface CljcService {
    /**
     * 查询方法
     *
     * @return
     */


    R list(CljcDto cljcDto);
}
