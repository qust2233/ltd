package com.itqf.daofa.Service;

import com.itqf.daofa.Dto.DaofaDto;
import com.itqf.daofa.pojo.Daofa;
import com.itqf.daofa.vo.R;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:32
 * description:
 */
public interface DaofaService {


    /**
     * 删除数据方法
     *
     * @param did
     * @return
     */
    R delete(int did);

    /**
     * 添加数据方法
     *
     * @param daofa
     * @return
     */
    R save(Daofa daofa);




    /**
     * 查询方法
     *
     * @return
     */
    R findlist(DaofaDto daofaDto);
}
