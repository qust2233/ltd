package com.itqf.daofa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.itqf.daofa.mapper")
public class DaofaApplication {

    public static void main(String[] args) {
        SpringApplication.run(DaofaApplication.class, args);
    }

}
