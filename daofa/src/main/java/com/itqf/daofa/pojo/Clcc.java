package com.itqf.daofa.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:16
 * description: 车辆出场
 */
@Data
public class Clcc {
    @TableId(type = IdType.AUTO)
    private Integer caccount;
    private String  cid;
    private Date cdate;
    private String ctype;
    private String ccaccount;
    private String jpassageway;
    private Double yprice;
    private Double sprice;
    private String jcomment;
}
