package com.itqf.daofa.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;


import java.sql.Time;
import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 11:40
 * description:在场记录
 */
@Data
public class Zcjl {
    @TableId(type = IdType.AUTO)
    private Integer cid;
    private Date jdate;
    private String ctype;
    private String tcc;
    private Time  tctime;
    private Double tcprice;
}
