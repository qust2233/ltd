package com.itqf.daofa.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 10:16
 * description: 道闸设备
 */
@Data
public class Daofa {
    @TableId(type = IdType.AUTO)
    private Integer did;
    private String dname;
    private String dfbh;
    private String dfip;
    private String bbh;
    private String fx;
    private String cs;
    private Integer deleted;

}
