package com.itqf.daofa.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:24
 * description:道闸协议
 */
@Data
public class Dfxy {
    @TableId(type = IdType.AUTO)
    private Integer zaccount;
    private Integer csaccount;
    private String csname;
    private String xybb;
    private String kfz;
}
