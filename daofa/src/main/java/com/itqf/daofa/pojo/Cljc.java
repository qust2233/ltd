package com.itqf.daofa.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:22
 * description:车辆进场
 */
@Data
public class Cljc {
    @TableId(type = IdType.AUTO)
    private Integer caccount;
    private String  cid;
    private Date jdate;
    private String ctype;
    private String jaccount;
    private String jpassageway;
    private String jcomment;
}
