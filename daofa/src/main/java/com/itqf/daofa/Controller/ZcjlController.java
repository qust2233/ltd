package com.itqf.daofa.Controller;

import com.itqf.daofa.Service.DaofaService;
import com.itqf.daofa.Service.ZcjlService;
import com.itqf.daofa.pojo.Daofa;
import com.itqf.daofa.pojo.Zcjl;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 15:21
 * description:
 */
@RestController
@CrossOrigin("*")
@RequestMapping("zcjl")
public class ZcjlController {
    @Autowired
    private ZcjlService zcjlService;
    @PostMapping("save")
    public Object save(@RequestBody Zcjl zcjl){
        R r = zcjlService.save(zcjl);
        return r;
    }
}
