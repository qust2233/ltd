package com.itqf.daofa.Controller;


import com.itqf.daofa.Dto.CljcDto;
import com.itqf.daofa.Service.CljcService;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 16:09
 * description:
 */
@RestController
@CrossOrigin("*")
@RequestMapping("cljc")
public class CljcController {



    @Autowired
    private CljcService cljcService;



   @GetMapping("list")
    public Object list( CljcDto cljcDto){

        R r = cljcService.list(cljcDto);


       return r;
  }
}
