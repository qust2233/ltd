package com.itqf.daofa.Controller;

import com.itqf.daofa.Dto.ClccDto;
import com.itqf.daofa.Dto.CljcDto;
import com.itqf.daofa.Service.ClccService;
import com.itqf.daofa.Service.CljcService;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 11:43
 * description:
 */
@RestController
@CrossOrigin("*")
@RequestMapping("clcc")
public class ClccController {


    @Autowired
    private ClccService clccService;

    @GetMapping("list")
    public Object list(ClccDto clccDto){

        R r = clccService.clist(clccDto);


        return r;
    }
}
