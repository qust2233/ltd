package com.itqf.daofa.Controller;

import com.itqf.daofa.Dto.ClccDto;
import com.itqf.daofa.Dto.DfxyDto;
import com.itqf.daofa.Service.ClccService;
import com.itqf.daofa.Service.DfxyService;
import com.itqf.daofa.pojo.Dfxy;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/29 12:49
 * description:
 */
@RestController
@CrossOrigin("*")
@RequestMapping("dfxy")
public class DfxyController {
    @Autowired
    private DfxyService dfxyService;

    @GetMapping("dlist")
    public Object list(DfxyDto dfxyDto){

        R r = dfxyService.dlist(dfxyDto);


        return r;
    }
}
