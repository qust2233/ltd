package com.itqf.daofa.Controller;

import com.itqf.daofa.Dto.DaofaDto;
import com.itqf.daofa.Service.DaofaService;
import com.itqf.daofa.pojo.Daofa;
import com.itqf.daofa.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: ltd
 *
 * @author: 曹景鲁
 * time: 2021/9/28 14:39
 * description:
 */
@RestController
@CrossOrigin("*")
@RequestMapping("daofa")
public class DaofaController {
    @Autowired
    private DaofaService daofaService;

    /**
     *添加数据
     * @param daofa
     * @return
     */
    @PostMapping("save")
    public Object save(@RequestBody Daofa daofa){
        R r = daofaService.save(daofa);
        return r;
    }
    /**
     * 删除方法
     * @param did
     * @return
     */
    @GetMapping("delete")
    public Object delete(int did){
        R r = daofaService.delete(did);
        return r;
    }
    /**
     *模糊查询
     * @param daofaDto
     * @return
     */
    @PostMapping("findlist")
    public Object findlist(DaofaDto daofaDto){
        R r = daofaService.findlist(daofaDto);

        return r;
    }

}

