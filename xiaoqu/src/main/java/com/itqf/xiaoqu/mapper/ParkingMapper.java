package com.itqf.xiaoqu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.xiaoqu.pojo.Parking;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 15:30
 * description:
 */
public interface ParkingMapper extends BaseMapper<Parking> {


}
