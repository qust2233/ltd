package com.itqf.xiaoqu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.xiaoqu.pojo.Xiaoqu;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 14:23
 * description:
 */

public interface XiaoquMapper extends BaseMapper<Xiaoqu> {


}
