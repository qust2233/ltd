package com.itqf.xiaoqu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.xiaoqu.pojo.Xiaoquperson;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 13:00
 * description:
 */
public interface XiaoqupersonMapper extends BaseMapper<Xiaoquperson> {
}
