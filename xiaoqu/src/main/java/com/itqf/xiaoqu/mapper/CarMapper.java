package com.itqf.xiaoqu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.xiaoqu.pojo.Car;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 13:44
 * description:
 */
public interface CarMapper extends BaseMapper<Car> {
}
