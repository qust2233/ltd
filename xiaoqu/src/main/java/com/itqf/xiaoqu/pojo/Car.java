package com.itqf.xiaoqu.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.DateTimeException;
import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 14:00
 * description:
 */
@Data
public class Car {

    @TableId(type = IdType.AUTO)
    private Integer cid;

    @TableField("carID")
    private String carID;

    @TableField("lianxiren")
    private String lianxiren;

    @TableField("lianxiPhone")
    private Integer lianxiPhone;

    @TableField("carWID")
    private String carWID;

    @TableField("startDate")
    private Date startDate;

    @TableField("stopDate")
    private Date stopDate;

    @TableField("deleted")
    private Integer deleted;


}
