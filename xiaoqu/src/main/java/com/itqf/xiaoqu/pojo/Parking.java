package com.itqf.xiaoqu.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.DateTimeException;
import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 13:57
 * description:
 */
@Data
public class Parking {
    @TableId(type = IdType.AUTO)
    private Integer parkingId;

    @TableField("parkingEncode")
    private String parkingEncode;

    @TableField("parkingWID")
    private Integer parkingWID;

    @TableField("parkingDate")
    private Date parkingDate;

    @TableField("deleted")
    private Integer deleted;



}
