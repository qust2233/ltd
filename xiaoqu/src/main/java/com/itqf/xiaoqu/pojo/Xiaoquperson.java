package com.itqf.xiaoqu.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.DateTimeException;
import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 13:53
 * description:
 */
@Data
public class Xiaoquperson {


    @TableId(type = IdType.AUTO)
    private Integer xiaoquPersonId;

    @TableField("xiaoquPersonFace")
    private String xiaoquPersonFace;

    @TableField("xiaoquPersonName")
    private String xiaoquPersonName;

    @TableField("xiaoquPersonPhone")
    private String xiaoquPersonPhone;

    @TableField("xiaoquPersonIDCard")
    private Integer xiaoquPersonIDCard;

    @TableField("xiaoquPersonType")
    private String xiaoquPersonType;

    @TableField("xiaoquPersonWID")
    private Integer xiaoquPersonWID ;

    @TableField("xiaoquPersonDate")
    private Date xiaoquPersonDate ;

    @TableField("deleted")
    private Integer  deleted;

}
