package com.itqf.xiaoqu.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.DateTimeException;
import java.util.Date;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 13:36
 * description:
 */
@Data
public class Xiaoqu {

    @TableId(type = IdType.AUTO)
    private Integer xiaoquId;
    private String xiaoquName;
    private String city;


    @TableField("xiaoquWid")
    private Integer xiaoquWid;

    private String xiaoquEncode;
    private String xiaoquAddress;
    private Date xiaoquDate;
    private Integer deleted;

}
