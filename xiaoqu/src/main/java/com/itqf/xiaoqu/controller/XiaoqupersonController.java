package com.itqf.xiaoqu.controller;

import com.itqf.xiaoqu.dto.XiaoquDto;
import com.itqf.xiaoqu.dto.XiaoqupersonDto;
import com.itqf.xiaoqu.pojo.Xiaoqu;
import com.itqf.xiaoqu.pojo.Xiaoquperson;
import com.itqf.xiaoqu.service.XiaoqupersonService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 12:57
 * description:
 */
@RestController
@CrossOrigin("*")

@RequestMapping("/xiaoquperson")
public class XiaoqupersonController {
    @Autowired
    private XiaoqupersonService xiaoqupersonService;


    @PostMapping("list")
    public Object list(@RequestBody XiaoqupersonDto xiaoqupersonDto){


        R r = xiaoqupersonService.list(xiaoqupersonDto);

        return r;
    }


    @PostMapping("save")
    public Object save(@RequestBody Xiaoquperson xiaoquperson){

        R r = xiaoqupersonService.save(xiaoquperson);

        return  r;
    }

    @PostMapping("update")
    public Object update(@RequestBody Xiaoquperson xiaoquperson){
        R r = xiaoqupersonService.update(xiaoquperson);
        return r;
    }

    @GetMapping("delete")
    public Object delete(int xid){
        R r = xiaoqupersonService.delete(xid);
        return r;

    }




}
