package com.itqf.xiaoqu.controller;

import com.itqf.xiaoqu.dto.ParkingDto;
import com.itqf.xiaoqu.dto.XiaoquDto;
import com.itqf.xiaoqu.pojo.Parking;
import com.itqf.xiaoqu.pojo.Xiaoqu;
import com.itqf.xiaoqu.service.ParkingService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 15:28
 * description:
 */
@RestController
@CrossOrigin("*")

@RequestMapping("/parking")
public class ParkingController {


    @Autowired
    private ParkingService parkingService;

    @PostMapping("list")
    public Object list(@RequestBody ParkingDto parkingDto){


        R r = parkingService.list(parkingDto);

        return r;
    }
    @PostMapping("save")
    public Object save(@RequestBody Parking parking){

        R r = parkingService.save(parking);

        return  r;
    }

    @PostMapping("update")
    public Object update(@RequestBody Parking parking){
        R r = parkingService.update(parking);
        return r;
    }

    @GetMapping("delete")
    public Object delete(int xid){
        R r = parkingService.delete(xid);
        return r;

    }



}
