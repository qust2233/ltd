package com.itqf.xiaoqu.controller;

import com.itqf.xiaoqu.dto.XiaoquDto;
import com.itqf.xiaoqu.pojo.Car;
import com.itqf.xiaoqu.pojo.Xiaoqu;
import com.itqf.xiaoqu.service.CarService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 13:43
 * description:
 */

@RestController
@CrossOrigin("*")

@RequestMapping("/car")
public class CarController {


    @Autowired
    private CarService carService;


    @GetMapping("list")
    public Object list(String key){


        R r = carService.list(key);

        return r;
    }
//    @PostMapping("save")
//    public Object save(@RequestBody Car car){
//
//        R r = carService.save(car);
//
//        return  r;
//    }
//
//    @PostMapping("update")
//    public Object update(@RequestBody Car car){
//        R r = carService.update(car);
//        return r;
//    }

    @GetMapping("delete")
    public Object delete(int xid){
        R r = carService.delete(xid);
        return r;

    }






}
