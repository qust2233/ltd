package com.itqf.xiaoqu.controller;

import com.itqf.xiaoqu.dto.XiaoquDto;
import com.itqf.xiaoqu.mapper.XiaoquMapper;
import com.itqf.xiaoqu.pojo.Xiaoqu;
import com.itqf.xiaoqu.service.XiaoquService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 14:25
 * description:小区信息controller
 */
@RestController
@CrossOrigin("*")

@RequestMapping("xiaoqu")
public class XiaoquController {


    @Autowired
    private XiaoquService xiaoquService;



    @PostMapping("list")
    public Object list(@RequestBody XiaoquDto xiaoquDto){
        System.out.println("xiaoquDto ------------------= " + xiaoquDto);

        R r = xiaoquService.list(xiaoquDto);

        return r;
    }
    @PostMapping("save")
    public Object save(@RequestBody Xiaoqu xiaoqu){

        R r = xiaoquService.save(xiaoqu);

        return  r;
    }

    @PostMapping("update")
    public Object update(@RequestBody Xiaoqu xiaoqu){
        R r = xiaoquService.update(xiaoqu);
        return r;
    }

    @GetMapping("delete")
    public Object delete(int xid){
        R r = xiaoquService.delete(xid);
        return r;

    }


}
