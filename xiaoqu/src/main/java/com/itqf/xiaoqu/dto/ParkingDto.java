package com.itqf.xiaoqu.dto;

import lombok.Data;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 15:30
 * description:
 */
@Data
public class ParkingDto {

    private String parkingid;
    private String parkingencode;


}
