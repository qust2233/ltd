package com.itqf.xiaoqu.dto;

import lombok.Data;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 10:05
 * description:
 */
@Data
public class XiaoquDto {

    private  String key;
    private  String xiaoquEncode;


}
