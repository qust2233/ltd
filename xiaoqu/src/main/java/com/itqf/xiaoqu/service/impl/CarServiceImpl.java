package com.itqf.xiaoqu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.xiaoqu.mapper.CarMapper;
import com.itqf.xiaoqu.pojo.Car;
import com.itqf.xiaoqu.pojo.Xiaoquperson;
import com.itqf.xiaoqu.service.CarService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 13:46
 * description:
 */
@Service
@Transactional
public class CarServiceImpl implements CarService {


    @Autowired
    private CarMapper carMapper;

    @Override
    public R list(String key) {
        QueryWrapper<Car> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("deleted",0);
        if(key !=null && key != ""){
            queryWrapper.like("carID",key);
        }
        List<Car> users = carMapper.selectList(queryWrapper);
        return R.OK("查询成功",users);

    }

    @Override
    public R delete(int xid) {
        Car car = new Car();
        car.setCid(xid);
        car.setDeleted(1);

        int row = carMapper.updateById(car);

        return R.OK("删除成功！！！",row);
    }
}
