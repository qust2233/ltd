package com.itqf.xiaoqu.service;

import com.itqf.xiaoqu.dto.XiaoquDto;
import com.itqf.xiaoqu.pojo.Xiaoqu;
import com.itqf.xiaoqu.vo.R;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 15:06
 * description:
 */

public interface XiaoquService  {
  

    R save(Xiaoqu xiaoqu);


    R list(XiaoquDto xiaoquDto);

    R update(Xiaoqu xiaoqu);

    R delete(int xid);
}
