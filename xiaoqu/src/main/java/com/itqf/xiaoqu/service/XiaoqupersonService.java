package com.itqf.xiaoqu.service;

import com.itqf.xiaoqu.dto.XiaoqupersonDto;
import com.itqf.xiaoqu.pojo.Xiaoquperson;
import com.itqf.xiaoqu.vo.R;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 12:58
 * description:
 */
public interface XiaoqupersonService {
    R list(XiaoqupersonDto xiaoqupersonDto);

    R save(Xiaoquperson xiaoquperson);

    R update(Xiaoquperson xiaoquperson);

    R delete(int xid);
}
