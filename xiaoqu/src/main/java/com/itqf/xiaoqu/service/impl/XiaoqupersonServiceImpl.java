package com.itqf.xiaoqu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.xiaoqu.dto.XiaoqupersonDto;
import com.itqf.xiaoqu.mapper.XiaoqupersonMapper;
import com.itqf.xiaoqu.pojo.Xiaoqu;
import com.itqf.xiaoqu.pojo.Xiaoquperson;
import com.itqf.xiaoqu.service.XiaoqupersonService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 12:59
 * description:
 */
@Transactional
@Service
public class XiaoqupersonServiceImpl implements XiaoqupersonService {




    @Autowired
    private XiaoqupersonMapper xiaoqupersonMapper;




    @Override
    public R list(XiaoqupersonDto xiaoqupersonDto) {
        QueryWrapper<Xiaoquperson> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("deleted","0");
        if (xiaoqupersonDto.getPersonname()!=null&&xiaoqupersonDto.getPersonname()!=""){
            queryWrapper.like("xiaoquPersonName", xiaoqupersonDto.getPersonname());
        }
        if (xiaoqupersonDto.getPersontype()!=null&xiaoqupersonDto.getPersontype()!=""){
            queryWrapper.like("xiaoquPersonType",xiaoqupersonDto.getPersontype());
        }

        List<Xiaoquperson> xiaoqupeople = xiaoqupersonMapper.selectList(queryWrapper);

        return R.OK("查询成功！",xiaoqupeople);
    }

    @Override
    public R save(Xiaoquperson xiaoquperson) {
        int rows=0;
        try {
            rows=xiaoqupersonMapper.insert(xiaoquperson);
        }catch (Exception e){}
        if (rows == 0) {
            return R.FAIL("添加失败!");
        }
        return R.OK("添加成功！");
    }

    @Override
    public R update(Xiaoquperson xiaoquperson) {
        int rows=0;
        try {
            rows=xiaoqupersonMapper.updateById(xiaoquperson);
        }catch (Exception e){}
        if (rows == 0) {
            return R.FAIL("修改失败!");
        }
        return R.OK("修改成功！");
    }

    @Override
    public R delete(int xid) {
        Xiaoquperson  xiaoquperson = new Xiaoquperson();
        xiaoquperson.setXiaoquPersonId(xid);
        xiaoquperson.setDeleted(1);

        int row = xiaoqupersonMapper.updateById(xiaoquperson);

        return R.OK("删除成功！！！",row);
    }
}
