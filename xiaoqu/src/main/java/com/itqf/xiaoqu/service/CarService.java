package com.itqf.xiaoqu.service;

import com.itqf.xiaoqu.vo.R;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 13:46
 * description:
 */
public interface CarService {
    R list(String key);

    R delete(int xid);
}
