package com.itqf.xiaoqu.service;

import com.itqf.xiaoqu.dto.ParkingDto;
import com.itqf.xiaoqu.pojo.Parking;
import com.itqf.xiaoqu.vo.R;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 15:38
 * description:
 */
public interface ParkingService {
    R list(ParkingDto parkingDto);

    R save(Parking parking);

    R update(Parking parking);

    R delete(int xid);
}
