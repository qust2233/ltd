package com.itqf.xiaoqu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.xiaoqu.dto.XiaoquDto;
import com.itqf.xiaoqu.mapper.XiaoquMapper;
import com.itqf.xiaoqu.pojo.Xiaoqu;
import com.itqf.xiaoqu.service.XiaoquService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/28 15:12
 * description:
 */
@Transactional
@Service
public class xiaoquServiceImpl implements XiaoquService {

    @Autowired
    private  XiaoquMapper xiaoquMapper;





    @Override
    public R list(XiaoquDto xiaoquDto) {

        QueryWrapper<Xiaoqu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("deleted","0");
        if (xiaoquDto.getKey()!=null&&xiaoquDto.getKey()!=""){
            queryWrapper.like("xiaoqu_name", xiaoquDto.getKey());
        }
        if (xiaoquDto.getXiaoquEncode()!=null&xiaoquDto.getXiaoquEncode()!=""){
            queryWrapper.like("xiaoqu_encode",xiaoquDto.getXiaoquEncode());
        }

        List<Xiaoqu> xiaoqus = xiaoquMapper.selectList(queryWrapper);

        return R.OK("查询成功！",xiaoqus);
    }



    @Override
    public R save(Xiaoqu xiaoqu) {

        int rows=0;
        try {
            rows=xiaoquMapper.insert(xiaoqu);
        }catch (Exception e){}
        if (rows == 0) {
            return R.FAIL("添加失败!");
        }
        return R.OK("添加成功！");

    }
    @Override
    public R update(Xiaoqu xiaoqu) {
        int rows=0;
        try {
            rows=xiaoquMapper.updateById(xiaoqu);
        }catch (Exception e){}
        if (rows == 0) {
            return R.FAIL("修改失败!");
        }
        return R.OK("修改成功！");

    }

    @Override
    public R delete(int xid) {
        Xiaoqu  xiaoqu = new Xiaoqu();
        xiaoqu.setXiaoquId(xid);
        xiaoqu.setDeleted(1);

        int row = xiaoquMapper.updateById(xiaoqu);

        return R.OK("删除成功！！！",row);
    }


}
