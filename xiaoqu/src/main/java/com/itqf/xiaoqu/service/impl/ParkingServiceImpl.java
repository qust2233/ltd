package com.itqf.xiaoqu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.xiaoqu.dto.ParkingDto;
import com.itqf.xiaoqu.mapper.ParkingMapper;
import com.itqf.xiaoqu.pojo.Parking;
import com.itqf.xiaoqu.pojo.Xiaoquperson;
import com.itqf.xiaoqu.service.ParkingService;
import com.itqf.xiaoqu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * projectName: ltd
 *
 * @author: 王兆星
 * time: 2021/9/29 15:40
 * description:
 */
@Service
@Transactional
public class ParkingServiceImpl implements ParkingService {


    @Autowired
    private ParkingService parkingService;
    @Autowired
    private ParkingMapper parkingMapper;


    @Override
    public R list(ParkingDto parkingDto) {
        QueryWrapper<Parking> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("deleted","0");
        if (parkingDto.getParkingid()!=null&&parkingDto.getParkingid()!=""){
            queryWrapper.like("parking_id", parkingDto.getParkingid());
        }
        if (parkingDto.getParkingencode()!=null&parkingDto.getParkingencode()!=""){
            queryWrapper.like("ParkingEncode",parkingDto.getParkingencode());
        }

        List<Parking> parkings = parkingMapper.selectList(queryWrapper);

        return R.OK("查询成功！",parkings);
    }

    @Override
    public R save(Parking parking) {
        int rows=0;
        try {
            rows=parkingMapper.insert(parking);
        }catch (Exception e){}
        if (rows == 0) {
            return R.FAIL("添加失败!");
        }
        return R.OK("添加成功！");
    }

    @Override
    public R update(Parking parking) {
        int rows=0;
        try {
            rows=parkingMapper.updateById(parking);
        }catch (Exception e){}
        if (rows == 0) {
            return R.FAIL("修改失败!");
        }
        return R.OK("修改成功！");
    }

    @Override
    public R delete(int xid) {
        Parking  parking = new Parking();
        parking.setParkingId(xid);
        parking.setDeleted(1);

        int row = parkingMapper.updateById(parking);

        return R.OK("删除成功！！！",row);
    }
}
