package com.itqf.xiaoqu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

@MapperScan(basePackages = "com.itqf.xiaoqu.mapper")
public class XiaoquApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaoquApplication.class, args);
    }


}
