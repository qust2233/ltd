package com.itqf.xitongshezhi.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * projectName: springboot_part
 *
 * @author: 王颖
 * time: 2021/9/29 10:25
 * description:
 */
@Data
public class User {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String  account;
    private String password;
    private Integer type;
    private Integer deleted;

    private String name;
}