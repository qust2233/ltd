package com.itqf.xitongshezhi.service;

import com.itqf.xitongshezhi.dto.CommonsDto;
import com.itqf.xitongshezhi.dto.UserDto;
import com.itqf.xitongshezhi.pojo.User;
import com.itqf.xitongshezhi.vo.R;

public interface UserService {
    R login(UserDto userDto);

    R resetpwd(CommonsDto commonsDto);

    R findUserList(String key);

    R saveUser(User user);

    R removeUserById(int id);


}
