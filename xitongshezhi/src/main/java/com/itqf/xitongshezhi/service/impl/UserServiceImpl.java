package com.itqf.xitongshezhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.xitongshezhi.dto.CommonsDto;
import com.itqf.xitongshezhi.dto.UserDto;
import com.itqf.xitongshezhi.mapper.UserMapper;
import com.itqf.xitongshezhi.pojo.User;
import com.itqf.xitongshezhi.service.UserService;
import com.itqf.xitongshezhi.utils.StringUtils;
import com.itqf.xitongshezhi.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.List;

/**
 * projectName: springboot_part
 *
 * @author: 王颖
 * time: 2021/9/29 10:29
 * description:
 */
@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserMapper userMapper;

    //登录
    @Override
    public R login(UserDto userDto) {
        //1. user中包含账号和密码但是密码是明文(12345
        ////将密码进行md5加密+ sms 的盐
        ////2.根据账号进行数据库查询-
        //user
        ////3.如果user为null,证明账号错误!
        ////4.如果user 不为null,判断deleted是六为1
        ////如果为1逻辑删除的用户
        ////如果不为1,对比密码
        ////5.根据密码对比的结果返回R]

        if (userDto.getAccount()== null || userDto.getAccount()== ""){
            return R.FAIL("账号不能为null");

        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account",userDto.getAccount());


        User dbUser = userMapper.selectOne(queryWrapper);

        if (dbUser == null){
            return R.FAIL("账号错误");
        }
        if (dbUser.getDeleted()==1){

            return R.FAIL("账号已经被删除，请联系管理员恢复");
        }
        DigestUtils.md5DigestAsHex(userDto.getPassword().getBytes());
        String password= DigestUtils.md5DigestAsHex(userDto.getPassword().getBytes());
        if (!password.equals(dbUser.getPassword()))
        {
            return R.FAIL("密码错误");

        }

        return R.OK("登录成功",dbUser);
    }

        //修改密码
    @Override
    public R resetpwd(CommonsDto commonsDto) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //QueryWrapper<User>queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("account",commonsDto.getAccount());

        queryWrapper.eq("password", StringUtils.dealPassword(commonsDto.getOld()));
        User user = userMapper.selectOne(queryWrapper);

        if(user == null){
            return R.FAIL("原密码错误");

        }
        user.setPassword(StringUtils.dealPassword(commonsDto.getNow()));
        user.setAccount(null);
        userMapper.updateById(user);

        return R.OK("密码修改成功");
    }

    //查找
    @Override
    public R findUserList(String key) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("deleted",0);
        if (key != null && key != ""){
            queryWrapper.like("name",key);
        }
        List<User> users = userMapper.selectList(queryWrapper);
        return R.OK("查询成功",users);



    }

    //新增用户
    @Override

    public R saveUser(User user) {
        user.setPassword(StringUtils.dealPassword((user.getPassword())));
        int rows = 0;
        try {
            rows = userMapper.insert(user);

        }catch (Exception e){

        }
        if (rows == 0){
            return R.FAIL("保存用户失败");
        }
        return R.OK("保存用户成功");
    }

    //删除用户
    @Override
    public R removeUserById(int id) {
        User user = new User();
        user.setId(id);
        user.setDeleted(1);
        int rows = userMapper.updateById(user);
        if (rows == 0){
            return R.FAIL("用户删除失败");
        }


        return R.OK("用户删除成功");
    }


}
