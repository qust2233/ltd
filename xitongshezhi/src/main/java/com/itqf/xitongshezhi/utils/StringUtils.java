package com.itqf.xitongshezhi.utils;

import org.springframework.util.DigestUtils;

/**
 * projectName: untitled
 *
 * @author: 王颖
 * time: 2021/9/29 12:02
 * description:
 */
public class StringUtils {
    public  static String dealPassword(String pwd){

        String password = DigestUtils.md5DigestAsHex((pwd + "xiaoqu").getBytes());

        return password;
    }
}
