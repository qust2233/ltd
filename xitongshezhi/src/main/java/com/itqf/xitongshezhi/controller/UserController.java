package com.itqf.xitongshezhi.controller;

import com.itqf.xitongshezhi.dto.CommonsDto;
import com.itqf.xitongshezhi.dto.UserDto;
import com.itqf.xitongshezhi.pojo.User;
import com.itqf.xitongshezhi.service.UserService;
import com.itqf.xitongshezhi.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: springboot_part
 *
 * @author: 王颖
 * time: 2021/9/29 10:26
 * description:
 */
@RestController
@CrossOrigin("*")
@RequestMapping("user")

public class UserController {

    @Autowired
    private UserService userService;

//登录
    //@RequestMapping(value = "login",method = RequestMethod.POST)
    @PostMapping("login")
    public Object login(@RequestBody UserDto userDto){
        R r = userService.login(userDto);
        return r;
    }
    //修改密码
    @PostMapping ("resetpwd")
    public Object resetpwd(@RequestBody CommonsDto commonsDto){
        R r = userService.resetpwd(commonsDto);
        return r;
    }

    //查找
    @GetMapping("list")

    //@RequestMapping(value = "list",method = RequestMethod.GET)

    public Object list(String key){
        R r = userService.findUserList(key);
        return r;
    }

    //新增用户
    @PostMapping("save")
    public Object save(@RequestBody User user){
        R r = userService.saveUser(user);
        return r;
    }


    //删除用户
    @GetMapping("delete")

    public Object delete(int id){
        R r = userService.removeUserById(id);
        return r;
    }

}
