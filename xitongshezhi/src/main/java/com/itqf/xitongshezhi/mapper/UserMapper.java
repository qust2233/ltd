package com.itqf.xitongshezhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.xitongshezhi.pojo.User;

public interface UserMapper  extends BaseMapper<User> {
}
