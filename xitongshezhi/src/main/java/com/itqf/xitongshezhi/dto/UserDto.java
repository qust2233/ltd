package com.itqf.xitongshezhi.dto;

import lombok.Data;

/**
 * projectName: springboot_part
 *
 * @author: 王颖
 * time: 2021/9/29 11:15
 * description:
 */
@Data
public class UserDto {
    private String account;
    private String password;
}
